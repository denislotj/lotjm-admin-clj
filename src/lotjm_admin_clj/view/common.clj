(ns lotjm-admin-clj.view.common
  (:require [ring.util.response :as response]
            [stencil.core :as stencil]
            [lotjm-admin-clj.util.flash :as flash]
            [lotjm-admin-clj.middleware.context :as context]
            [lotjm-admin-clj.util.session :as session]))

;;; Context utils
(defn get-context-root
  []
  (context/get-context-root))

(defn wrap-context-root
  "Add web context to the path of URI"
  [path]
  (str (get-context-root) path))

;;; User utils
(defn restricted
  "Function for restricted part of the Web site. 
   Takes a predicate function and the handler to execute if predicate is true."
  [predicate handler & args]
  (if (predicate)
    (apply handler args)
    (response/redirect (wrap-context-root "/"))))

(defn authenticated?
  "Sample authentication function. Test if current user is not null."
  []
  (not (nil? (session/current-user))))

(defn admin?
  "Sample authorization function. Test if current user it admin."
  []
  (if-let [user (session/current-user)]
    (= :admin (:type user))))

;;; Layout
(defn- base-content [title body]
  {:context-root (context/get-context-root)
   :title title
   :body body})

(defn- user-nav-links [user]
  ;; (when (admin?)
  ;;   [{:link (wrap-context-root "/admin") :label "Administration"}
  ;;    {:link (wrap-context-root "/") :label "Foo"}]))
  [])

(defn- get-alerts []
  "Obtains non-empty alert messages from the flash, to render in they layout file"
  (let [types [:alert-success :alert-info :alert-warning :alert-error]
        each-alert (map #(vec [% (flash/get %)]) types)
        without-nils (into {} (remove (fn [[k v]] (nil? v)) each-alert))]
    (map (fn [[k v]] {:class (name k) :message v}) without-nils)))

(defn wrap-layout
  "Define pages layout"
  [title body]
  (stencil/render-file
   "lotjm_admin_clj/view/templates/layout"
   (let [content (base-content title body)
         content (merge content {:alerts (get-alerts)}) ;; rebinding FTW
         user (session/current-user)]
     (if (authenticated?)
       (assoc content
         :authenticated?
         {:user (:login user)
          :nav-links (user-nav-links user)})
       content))))
