(ns lotjm-admin-clj.view.auth
  (:require [ring.util.response :as response]
            [compojure.core :refer [defroutes GET POST]]
            [stencil.core :as stencil]
            [lotjm-admin-clj.model.application-info :as appinfo]
            [lotjm-admin-clj.util.flash :as flash]
            [lotjm-admin-clj.util.session :as session]
            [lotjm-admin-clj.view.common :refer [wrap-context-root get-context-root wrap-layout]]))

(defn init-test-data
  "Initialise session with dummy data"
  []
  (session/set-user! {:login "admin" :type :admin}))

(defn- login-page-body [request companyname]
  (let [appdata (appinfo/by-name companyname)]
    (stencil/render-file
     "lotjm_admin_clj/view/templates/auth"
     {:context-root (get-context-root) :appdata appdata})))

(defn- login-page [request companyname]
  (if (session/current-user)
    (response/redirect (wrap-context-root "/"))
    (wrap-layout "Log in" (login-page-body request companyname))))

(defn- login [request]
  (let [appid (Integer. (:appid (:params request)))
        application (appinfo/by-id appid)]
    (init-test-data)
    (session/set-user!
     (merge (session/current-user)
            {:appname (:name application)}
            (select-keys (:params request) [:appid :login])))
    (println (session/current-user))
    (flash/put! :alert-info "Welcome!")
    (response/redirect (wrap-context-root "/"))))

(defn- logout [request]
  (let [appname (:appname (session/current-user))]
    (session/logout)
    (flash/put! :alert-success "You have logged out")
    (response/redirect (wrap-context-root (str "/applications/" appname)))))

(defroutes auth-routes
  (GET "/login/:company" [company :as request] (login-page request company))
  (GET "/applications/:company" [company :as request] (login-page request company))
  (POST "/login" request (login request))
  (GET "/logout" request (logout request)))
