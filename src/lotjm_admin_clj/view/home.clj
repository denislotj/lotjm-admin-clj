(ns lotjm-admin-clj.view.home
  (:require [compojure.core :refer [defroutes GET]]
            [stencil.core :as stencil]
            [lotjm-admin-clj.view.common :refer [wrap-layout]]))

(defn- page-body []
  (stencil/render-file
   "lotjm_admin_clj/view/templates/home"
   {}))

(defn- render-page [request]
  (wrap-layout "Home"
               (page-body)))

(defroutes home-routes
  (GET "/" request (render-page request)))
