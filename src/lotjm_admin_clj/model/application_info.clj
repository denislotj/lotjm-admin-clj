(ns lotjm-admin-clj.model.application-info
  (:use [korma.db]
        [korma.core]))

;; TODO move this to a global config OR use JNDI
(defdb db (mysql {:db "dtree" :user "root" :password "root"}))

(defentity application
  (table :application_info)
  (pk :application_id))

(defn by-id [id]
  (first
   (select
    application
    (fields [:application_id :id][:name :name])
    (where {:application_id [= id]}))))

(defn by-name [name]
  (first
   (select application
           (fields [:application_id :id][:name :name])
           (where {:name [= name]}))))
