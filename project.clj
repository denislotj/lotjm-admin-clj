(defproject lotjm-admin-clj "0.1.0-SNAPSHOT"
  :description "Backend administration site for LOTJm"
  :url "http://www.lotj.com/"
  :license {:name "Copyright 2002-2013 Law of the Jungle Pty Limited"}
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [ring "1.1.6"]
                 [compojure "1.1.3"]
                 [stencil "0.3.2"]
                 [korma "0.3.0-RC5"]
                 [mysql/mysql-connector-java "5.1.6"]
                 ;; required by korma:
                 [log4j "1.2.17"
                  :exclusions [javax.mail/mail
                               javax.jms/jms
                               com.sun.jdmk/jmxtools
                               com.sun.jmx/jmxri]]]

  :plugins [[lein-ring "0.8.6"]]
  :ring {:handler lotjm-admin-clj.app/site-handler}
  :war-resources-path "resources/public"
  :main lotjm-admin-clj.server)
